package com.zrr2.customhandshakedata.asm;

import net.minecraft.launchwrapper.IClassTransformer;
import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.tree.*;

public class ClassTransformer implements IClassTransformer {

    private static final String CLASS_NAME = "net.minecraft.network.handshake.client.C00Handshake";

    //The new handshake data sent
    private static final String NEW_HANDSHAKE_DATA = "\00\00Hello Goodbye\00\00";

    @Override
    public byte[] transform(String name, String transformedName, byte[] basicClass) {
        if (name.equals(CLASS_NAME) || transformedName.equals(CLASS_NAME)) {
            System.out.println("Found class " + CLASS_NAME);
            return patch(basicClass);
        }
        return basicClass;
    }

    private byte[] patch(byte[] basicClass) {
        ClassNode classNode = new ClassNode();
        ClassReader classReader = new ClassReader(basicClass);
        classReader.accept(classNode, 0);
        for (MethodNode method : classNode.methods) {
            InsnList insnList = new InsnList();
            for (AbstractInsnNode node : method.instructions.toArray()) {
                if (node instanceof LdcInsnNode && "\0FML\0".equals(((LdcInsnNode) node).cst)) {
                    ((LdcInsnNode) node).cst = NEW_HANDSHAKE_DATA;
                    System.out.println("Found one LDC node of \\00FML\\00");
                }
                insnList.add(node);
            }
            method.instructions.clear();
            method.instructions.add(insnList);
        }

        ClassWriter classWriter = new ClassWriter(ClassWriter.COMPUTE_MAXS);
        classNode.accept(classWriter);
        return classWriter.toByteArray();
    }
}
