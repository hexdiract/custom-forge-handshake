package com.zrr2.customhandshakedata;

import net.minecraftforge.fml.common.Mod;

@Mod(modid = CustomHandshakeDataMod.MODID, version = CustomHandshakeDataMod.VERSION)
public class CustomHandshakeDataMod {
    public static final String MODID = "customhandshakedata";
    public static final String VERSION = "1.0";

    /**
     * The class is basically useless. The class that does the magic is  ./asm/ClassTransformer
     * C00Handshake#hasFMLMarker can be used to check if the C00Handshake packet contains the extra bits of predefined data,
     */
}
